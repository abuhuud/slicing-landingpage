<!-- Hero Section -->
<section class="jumbotron bg-grey">
    <div class="container py-5">
      <div class="row">
        <div class="col-sm">
          <h1>Digitalisasi bisnis anda bersama <span class="font-semibold font-blue">warungweb.id</span></h1>
          <p class="mt-3"><span class="font-semibold font-blue">Warungweb</span> adalah penyedia jasa pembuatan website
            seperti
            landingpage, e-commerce, company profile.
            <br>
            <span class="font-semibold font-blue">Warungweb</span> juga menerima jasa desain banner, feed instagram,
            undangan digital, serta materi iklan lainnya.
          </p>
          <button class="btn bg-blue shadow mt-3 px-3 py-1">
            <a class="nav-link text-white font-semibold" href="#">Hubungi Kami</a>
          </button>
        </div>
        <div class="col-sm">
          <img src="<?= base_url()?>assets/img/banner.svg" width="500px" alt="">
        </div>
      </div>
    </div>
</section>
  <!-- End Of Hero Section -->

<!-- Layanan Kami -->
<section class="layanan" style="background-image: url(<?= base_url()?>assets/img/bg-pattern.png)">
    <div class="container py-5" >
      <h1>Layanan Kami</h1>
      <div class="row">
        <div class="col-sm py-5">
          <img src="<?= base_url()?>assets/img/web-icon.svg" width="75px" alt="">
          <h1 class="heading font-semibold py-2 font-blue">Pembuatan Website</h1>
          <p class="content font-light">Jadikan bisnis anda lebih profesional dengan website, kami menyediakan beberapa
            paket
            pilihan yang dapat
            disesuaikan dengan kebutuhan anda !</p>
        </div>
        <div class="col-sm py-5">
          <img src="<?= base_url()?>assets/img/design-icon.svg" width="75px" alt="">
          <h1 class="heading font-semibold py-2 font-blue">Desain</h1>
          <p class="content font-light">Buat tampilan bisnis anda lebih menarik dengan desain yang sesuai dengan brand
            bisnis anda.</p>
        </div>
        <div class="col-sm py-5">
          <img src="<?= base_url()?>assets/img/chart-icon.svg" width="75px" alt="">
          <h1 class="heading font-semibold py-2 font-blue">Digital Marketing</h1>
          <p class="content font-light">Tingkatkan omzet bisnis anda dengan marketer berpengalaman, just stay at home
            and cuan, cuan, cuan !</p>
        </div>
      </div>
    </div>
  </section>
  <!-- End Of Layanan Kami -->

  <!-- Tentang Kami -->
  <section class="tentang bg-grey">
    <div class="container py-5">
      <h1>Tentang Kami</h1>
      <div class="row">
        <div class="col-sm py-5">
          <p class="content font-light">
            Sejak 2019, <span class="font-semibold font-blue">Warungweb</span> telah dipercaya oleh UKM, UMKM serta
            Korporasi untuk meningkatkan bisnis mereka
            melalui
            pengembangan Website, Desain serta Digital Marketing. Konsultasikan bisnis anda kepada kami sekarang juga !
          </p>
          <button class="btn bg-blue shadow mt-3 px-3 py-1">
            <a class="nav-link text-white font-semibold" href="#">Hubungi Kami</a>
          </button>
        </div>
        <div class="col-sm">
          <img src="<?= base_url()?>assets/img/about.svg" alt="" class="my-n5">
        </div>
      </div>
    </div>

  </section>
  <!-- End Of Tentang Kami -->

  <!-- Klien Kami -->
  <section class="klien" style="background-image: url(<?= base_url()?>assets/img/bg-pattern.png)">
    <div class="container py-5">
      <h1>Klien Kami</h1>
      <div class="row modal-dialog-centered">
        <div class="col-sm py-5">
          <img src="<?= base_url()?>assets/img/client3.png" width="300px" alt="">
        </div>
        <div class="col-sm py-5">
          <img src="<?= base_url()?>assets/img/client1.png" width="480px" alt="">
        </div>
        <div class="col-sm py-5">
          <img src="<?= base_url()?>assets/img/client2.jpg" width="260px" alt="">
        </div>
      </div>
    </div>

  </section>
  <!-- End Of Klien Kami -->

  <!-- Testimoni -->
  <section class="testimoni bg-grey">
    <div class="container py-5">
      <h1>Testimoni</h1>
      <div class="row py-5 text-center">
        <div class="card-body col-sm">
          <div class="card p-3 shadow">
            <p class="content">Pembuatan website e-commerce cepat dan murah banget ! Udah gitu
              dapet fitur cek ongkir
              sampe checkout dapet
              notif. Mantep pokoknya ! <br><br>
              <Strong>Wisnu - Jogja</Strong> <br>
              CEO Cheapjack & Cheap Workshop
            </p>
          </div>
        </div>
        <div class="card-body col-sm">
          <div class="card p-3 shadow">
            <p class="content">Warungweb emang the best, website yang dibuat responsive disemua
              device.
              <br><br>
              <Strong>Robert - Jakarta</Strong> <br>
              Owner Rascar4x4 & Rastani
            </p>
          </div>
        </div>

        <div class="card-body col-sm">
          <div class="card p-3 shadow">
            <p class="content">Buat website di warungweb bikin bisnis makin lancar, karena client minta
              harus ada website dan email dengan domain usaha
              <br><br>
              <Strong>Arief - Jakarta</Strong> <br>
              Owner Kemasjaya
            </p>
          </div>
        </div>

      </div>
    </div>

  </section>
  <!-- End Of Testimoni-->