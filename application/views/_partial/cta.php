<!-- CTA -->
<section class="cta">
    <div class="container p-5 text-center text-white mt-5" style="background-image: url(<?= base_url()?>assets/img/invite-bg.svg)" id="cta">

      <h1>Ayo buruan tunggu apalagi !!! <br>
        Digitalisasikan bisnis anda bersama kami !</h1>
      <h3>Konsultasikan segera kebutuhan bisnis anda</h3>
      <button class="btn bg-white shadow mt-3 px-3 py-1">
        <a class="nav-link font-bold font-blue px-3 py-1" href="#">Hubungi Kami</a>
      </button>
    </div>
</section>
  <!-- End Of CTA -->

  <script src="js/jquery-3.6.1.slim.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- <script src="libs/fontawesome/js/fontawesome.min.js"></script>
  <script src="libs/fontawesome/js/all.min.js"></script> -->


</html>