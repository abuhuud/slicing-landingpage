  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-grey sticky-top shadow-sm">
    <div class="container py-2">
      <a class="navbar-brand text-reset" href="#">
        <img src="<?= base_url()?>assets/img/ww-logo.png" alt="logo" width="75px">
        <strong>warungweb.id</strong>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item px-3">
            <a class="nav-link font-regular text-reset" href="#">Harga <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link font-regular text-reset" href="#">Layanan</a>
          </li>
          <li class="nav-item px-3">
            <a class="nav-link font-regular text-reset" href="#">Tentang</a>
          </li>
          <li class="nav-item pl-3">
            <button class="btn bg-blue shadow px-3 py-1">
              <a class="nav-link text-white font-semibold" href="#">Hubungi Kami</a>
            </button>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- End Of Navbar -->